#Global marco
%global __requires_exclude %{?__requires_exclude:%__requires_exclude|}^perl\\(XML::Parser\\)$

#Basic Information
Name:           perl-XML-XPath
Version:        1.48
Release:        2
Summary:        Parse and evaluate XPath statementsr
License:        Artistic-2.0 and (GPL-1.0-or-later OR Artistic-1.0-Perl)
URL:            https://metacpan.org/release/XML-XPath
Source0:        https://cpan.metacpan.org/authors/id/M/MA/MANWAR/XML-XPath-%{version}.tar.gz
BuildArch:      noarch

#Dependency
BuildRequires:  coreutils make
BuildRequires:  perl-generators perl-interpreter
BuildRequires:  perl(lib) perl(open) perl(overload)
BuildRequires:  perl(strict) perl(utf8) perl(vars) perl(warnings)
BuildRequires:  perl(Carp) perl(CPAN::Meta) perl(Data::Dumper)
BuildRequires:  perl(Exporter) perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires:  perl(IO::File) perl(Path::Tiny) >= 0.076
BuildRequires:  perl(POSIX) perl(Scalar::Util) perl(Test)
BuildRequires:  perl(Test::More) perl(XML::Parser) >= 2.23
Requires:       perl(POSIX) perl(warnings) perl(XML::Parser) >= 2.23

	
# perl-generators does not work properly for
#   "use parent qw/-norequire XML::XPath::Node/;"
%global __requires_exclude %{__requires_exclude}|^perl\\(-norequire\\)

%description
This module aims to comply exactly to the XPath specification at
http://www.w3.org/TR/xpath and yet allow extensions to be added in the
form of functions. Modules such as XSLT and XPointer may need to do
this as they support functionality beyond XPath.

%package_help

#Build sections
%prep
%autosetup -p1 -n XML-XPath-%{version}

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
make %{?_smp_mflags}

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
%{_fixperms} $RPM_BUILD_ROOT/*

install -d $RPM_BUILD_ROOT/%{_mandir}/man1/
echo .so man3/XML::XPath.3pm >> $RPM_BUILD_ROOT/%{_mandir}/man1/xpath.1 << EOF

%check
make test

#Files list
%files
%license LICENSE
%doc Changes README TODO
%{_bindir}/xpath
%{perl_vendorlib}/XML

%files help
%{_mandir}/man1/xpath*
%{_mandir}/man3/*.3*

%changelog
* Tue Jan 14 2025 Funda Wang <fundawang@yeah.net> - 1.48-2
- drop useless perl(:MODULE_COMPAT) requirement

* Wed Jan 08 2025 xu_ping <707078654@qq.com> - 1.48-1
- Upgrade to version 1.48
  * Support use XML::XPath some_version and modernize inheritance.
  * Add documentation about supported XPath functions.

* Tue Jun 14 2022 SimpleUpdate Robot <tc@openeuler.org> - 1.47-1
- Upgrade to version 1.47

* Fri Jan 29 2021 liudabo <liudabo1@huawei.com> - 1.44-1
- upgrade version to 1.44

* Wed Jan 15 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.42.0-6
- Remove buildrequires

* Wed Jan 08 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.42.0-5
- Package init

